<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LeadQuik</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css" />
    <style>
    .align-center {
display: flex;
align-items: center;
justify-content: center;
    }

    .margin-top {
        margin-top: 10%;
    }
    </style>
</head>

<body>
<div class="ui inverted attached menu">
        <div class="ui container">
                <div class="ui huge inverted menu">
                        <div class="header item">LeadQuik</div>
                        <div class="item">
                                <a href="/">Search</a>
                        </div>
                        <div class="item">
                            <a href="/user">Leads</a>
                        </div>
                    </div>
        </div>
</div>