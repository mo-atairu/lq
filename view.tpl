% include('head.tpl')
    
    <div class="ui container margin-top">
 
            <div class="ten wide column">
                <form action="/search" method="GET" class="ui form align-center">
                    <div class="inline fields">
                        <div class="ten wide field">
                            <input type="text" name="sterm" placeholder="doctor, mechanic, painter .." id="sterm" type="text" minlength="5" required>
                        </div>
                        <div class="four wide field">
                            <input type="text" name="sloc" placeholder="us zip code" id="sloc" type="number" pattern="[0-9]{5,}" required>
                        </div>
                        <div class="four wide field">
                                <input type="text" name="proxy" placeholder="proxy" id="sloc" type="text" pattern="^([0-9]{2,3}+\.[0-9]{2,3}+\.[0-9]{2,3})$" required>
                            </div>
                        <div class="two wide field">
                            <button class="ui button green" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

    </div>


<div class="ui container">


    <p class="align-center">If you are authorized to view this
        page, you have been provided with proxies.
        Running on Google Cloud. 
    </p>
        
</div>

    </div>


</body>

</html>