import gevent
from gevent import monkey
monkey.patch_all()
import psycogreen.gevent
psycogreen.gevent.patch_psycopg()
import psycopg2
import redis
from peewee import *
from urllib3.poolmanager import proxy_from_url
import urllib3
from math import floor
import uuid
import certifi
import json
import pendulum
from model import Lead, Search, db


r = redis.StrictRedis(host='localhost', port=6379, db=0)
viewer = r.pubsub(ignore_subscribe_messages=True)
# this is channelB
viewer.subscribe('channelA')


BASE_URL = 'https://api.yelp.com/v3/businesses/search?'
HEADERS = {"Authorization": ("Bearer n8O6Dbfpnaitxk2SoYFUVXrjPFR_" +
                             "M5HIB78AeWVlUT2-0b25X9brRjeLHx-eao6qQr_" +
                             "0uNxpucuq3fIAgTBpERMLHyV1UmsQr6ZNWOBfG8" +
                             "rWPUhFCNHhW2PushU0W3Yx")}
http = proxy_from_url('https://35.229.82.141:80',
                        cert_reqs='CERT_REQUIRED',
                        ca_certs=certifi.where())


def gen_nanoid():
    return str(uuid.uuid4())[-12:]


def yelp(newsearch):
    searchid, sterm, sloc, proxy = newsearch
    next_offset = [0]
    business = []

    while True:
        for offset in next_offset:
            URL = (f'{BASE_URL}term={sterm}&' +
                   f'location={sloc}&' +
                   f'offset={offset}&' +
                   'limit=50')           
            print(URL)
            try:
                response = http.request('GET', URL, headers=HEADERS)
                sresult = json.loads(response.data.decode('utf-8'))
                result = (sresult['businesses'], sresult['total'])
            except BaseException as e:
                return str(e)

            for lead in result[0]:
                business.append({'bname': lead['name'],
                                 'yelpurl': lead['url'].split('?')[0],
                                 'bphone': lead['phone'],
                                 'searchid': searchid,
                                 'nanoid': gen_nanoid()})

        if len(next_offset) == 1:
            _count = list(range(0, floor(result[1]/50)))
            count = [50 * n for n in _count]
            filter_ = filter(lambda n: (n > 0 and n < 101), count)
            next_offset = list(filter_)
        else:
            break

    with db.atomic():
        results = [lead for lead in business if len(lead['bphone']) > 1]
        Lead.insert_many(results).execute()

    # publish on channelB
    message = {"newsearch": {"searchid": searchid, "proxy": proxy}}
    m = json.dumps(message)
    r.publish('channelB', m)
    print(m)

def run_forever():
    while True:
        message = viewer.get_message()
        if not message:
            gevent.sleep(5)
        if message:
            try:
                m = json.loads(message['data'])['newsearch']
                newsearch = tuple(m.values())
                print(newsearch)
                g = gevent.spawn(yelp, newsearch)
                g.join()
            except Exception:
                pass


if __name__ == '__main__':
    run_forever()
